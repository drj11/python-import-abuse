# 2 modules, one file

Disrupt the idea that a file might contain only one module.

File contents:

    Preamble
    ...

    Contents of Module 1
    ...

    import ex; ex.port(vars(), "module1")

    Contents of Module 2
    ...


    import ex; ex.port(vars(), "module2")
    
    Natural Contents
    of This Module
    ...


## What is going on here?

Separate modules can be concatenated into a single file.
The text of each module has
a small "magic code" suffix that creates the module;
that's the `import ex; ex.port(vars(), "module1")` code
in the example above.

A preamble, at the start of the file, defines
the code that makes `ex.port()` possible.


## Sketch implementation

Two fun facts you should know before proceeding:

- a module in Python just another object;
- a module is not just a list of functions and so on,
  it is code that is executed from top to bottom.

Demonstration in [`dem.py`](dem.py).

The approach taken is that
the preamble defines the `ex.port` function.
This `ex.port` function copies the contents of its first argument
info a freshly created module, and then installs that module
under the name given as the second argument.
In fact it is a move not a copy operation;
`ex.port` also deletes (using `del`) references from its first argument.

`vars()` is the `dict` for this module,
it has everything that has been defined in this module up to that point.
Passing `vars()` to `ex.port()` effectively puts whatever has
been defined in this module so far into a another module.
Actually, if `ex.port()` is used more than once then
the `vars()` dictionary contains everything defined since
the last call to `ex.port()` (rather than everything defined
since the beginning of the module).

## A typical terminal session:

There isn't much in the actual `dem` module.
It has a `main()` function that prints out a greeting,
using a function defined in a utility module `ut`.
Of course all the code is in a single file.

    ; python -m dem
    hello hello hello world!
    ; python
    Python 3.10.6 (main, Aug 11 2022, 13:49:01) [Clang 13.0.0 (clang-1300.0.29.30)] on darwin
    Type "help", "copyright", "credits" or "license" for more information.
    >>> import dem
    >>> dem
    <module 'dem'>
    >>> dem.ut
    <module '(ut)'>
    >>> dem.ut.ex
    <module 'dem' from '/Users/drj/prj/pyimpabuse/dem.py'>
    >>> dem.main()
    hello hello hello world!
    >>> help(dem)
    Help on module dem:

    NAME
        dem - For good greeting, invoke with python -m dem
    ...


## Corner cases and wrinkles

A docstring in a module is merely a literal that
appears as the first object in a module.

In the arrangement described here,
this automatic definition of docstring does not work, because
the preamble appears first.
So no string is the first object.
An adequate workaround is to assign to `__doc__` explicitly.
Incidentally, this works for a regular module too.

A function defined in a module refers to other functions and
objects defined in that module via `globals`.
`globals` are not in fact truly global in any sense.
Each function defined in Python has a `__globals__` attribute
in which that function finds global references when it needs them.
Normally this is the module in which the function is defined
(technically it is a `dict` that is also the result of calling
`vars()` on that module).

Even if you think a function doesn't use globals, it probably does.
For example, if you write a call to `math.sin()` in your
function, then the reference to the `math` module is via a
global (unless you write `import math` _inside_ your function;
which is well defined and works, and i used to do it, but
it is deprecated by all reasonable Python coding styles).

When a function is `ex.port`ed to another module,
its `__globals__` attribute needs fixing up.
That's not possible, but it is possible to defined a new
function object that is a clone of the old one, but
with a different `__globals__` attribute.

Except that i'm pretty sure that this only works for top-level
functions, not for closures defined inside a function, and
not for class methods.


## Idealisation

You can think of _module import_ mode as a sort of compilation
(because it is).
Python code is compiled and executed from top to bottom.
The compiler places compiled objects (anything with an
assignment, a `def`, a `class`, and so on) in a `dict` that is
the modules contents.
This is the same dict that `vars()` returns.

When i first had this idea,
i wanted to redirect and replace the `dict`:
At the point of the switch (the call to `ex.port()`) the `dict`
that the compiler was currently compiling into would be placed
in another module, and a fresh `dict` would be switched in.
Pulling the rug from out under the compiler and
furnishing it with a new one.

That would be better because it would mean that functions
wouldn't need fixing up.
They would already have the correct `__globals__` attribute
regardless of where in the syntax tree they were defined.

But i couldn't find a way to do that.


## Fun facts

The function `ex.port` is used to port itself into the `ex` module.
That's how i discovered the function-globals bug.
Once it was ported, the `ex.port` broke because the globals it
needed (`types` and so on) were no longer present in its
`__globals__` dict.


# END
