# invoke carefully with python -m dem

import inspect
import sys
import types

# based on https://stackoverflow.com/q/48629236/242457
def rehome(function, globals):
    """
    Create a clone of function, that has different globals.
    The name "rehome" comes from the thinking of the module in
    which a function is defined, which is normally what its
    __globals__ is set to, as its "home".
    """

    new_function = types.FunctionType(
        function.__code__,
        globals,
        name=function.__name__,
        argdefs=function.__defaults__,
        closure=function.__closure__,
    )
    new_function.__dict__.update(function.__dict__)
    return new_function


def port(source, target):
    """Find everything defined in source (which is typically
    the result of calling vars() on a module),
    and move (port) it into a fresh module.
    The fresh module is then assigned to sys.modules[target],
    making it importable under that name.
    The references to the objects that are moved are
    del'd from source (hence, move).
    """

    dest = types.ModuleType(f"({target})")

    dels = set()
    for k, v in source.items():
        # Don't copy a thing if it would override
        # something already set in dest.
        if getattr(dest, k, None) is not None:
            continue
        # functions need their globals resetting to
        # the new module, otherwise they try to use
        # global variables that don't exist.
        if inspect.isfunction(v):
            v = rehome(v, vars(dest))
        setattr(dest, k, v)
        dels.add(k)
    sys.modules[target] = dest
    for k in dels:
        del source[k]


port(vars(), "ex")


## Start of Utility Module

def triple(x):
    return x * 3


import ex ; ex.port(vars(), "ut")

## Start of "this" Module

__doc__ = "For good greeting, invoke with python -m dem"

import ut


def main():
    greet = ut.triple(["hello"])
    print(*greet, "world!")

if __name__ == "__main__":
    main()
